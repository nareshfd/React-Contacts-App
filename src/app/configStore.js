import { createStore } from "redux";
import testReducer from "../Features/testArea/testReducer";

export const configStore = () => {
	const Store = createStore(testReducer);
	return Store;
};
