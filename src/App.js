import React, { Component, Fragment } from "react";
import Header from "./Features/User/Nav/Navbar/Header";
import Homepage from "./Features/Home/HomePage";
import "bootstrap/dist/css/bootstrap.min.css";
import "./App.css";
import { Container } from "semantic-ui-react";
import { Route } from "react-router-dom";
import EventDashboard from "./Features/Event/Eventdashboard/EventDashboard";
import EventForm from "./Features/Event/EventForm/EventForm";
import EventDetailedPage from "./Features/Event/EventDeatiled/EventDeatiledPage";
import UserDetailed from "./Features/User/UserDetailed/UserDetailed";
import PeopleDashBoard from "./Features/User/PeopleDashBoard/PeopleDashBoard";
import SettingsDasboard from "./Features/User/settings/SettingsDashboard";
import testArea from "./Features/testArea/testArea";

export default class App extends Component {
	render() {
		return (
			<div className='App main'>
				<div>
					<Route exact path='/' component={Homepage} />
					<Route
						path='/(.+)'
						render={() => (
							<Fragment>
								<Header branding='Contacts App' className='container' />
								<Container className='main pt-5 container mt-5'>
									<Route path='/events' component={EventDashboard} />
									<Route path='/events/:id' component={EventDetailedPage} />
									<Route path='/people' component={PeopleDashBoard} />
									<Route path='/people:id' component={UserDetailed} />
									<Route path='/Settings' component={SettingsDasboard} />
									<Route path='/creaEvent' component={EventForm} />
									<Route path='/test' component={testArea} />
								</Container>{" "}
							</Fragment>
						)}
					/>
				</div>
			</div>
		);
	}
}
