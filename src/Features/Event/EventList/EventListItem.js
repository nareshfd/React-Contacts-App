import React, { Component } from 'react';
import { Segment, Item, Icon, List, Button } from 'semantic-ui-react';
import EventAttentee from './EventAttentee';

class EventListItem extends Component {
	render() {
		const { event, selectEvent, deleteEvent } = this.props;
		return (
			<>
				<Segment.Group>
					<Segment>
						<Item.Group>
							<Item>
								<Item.Image size='tiny' circular src={event.hostPhotoURL} />
								<Item.Content>
									<Item.Header as='a'>{event.title}</Item.Header>
									<Item.Description>
										Hosted by {event.hostedBy}
									</Item.Description>
								</Item.Content>
							</Item>
						</Item.Group>
					</Segment>
					<Segment>
						<span>
							<Icon name='clock' className='pl-2' /> {event.date} |
							<Icon name='marker' className='pl-2' /> {event.city}
						</span>
					</Segment>
					<Segment secondary>
						<List horizontal>
							{event.attendee &&
								event.attendees.map(attendee => (
									<EventAttentee key={attendee.id} attentee={attendee} />
								))}
						</List>
					</Segment>
					<Segment clearing>
						<span>{event.description}</span>
						<div className='col-md-12'>
							<Button
								as='a'
								color='red'
								floated='right'
								content='Delete'
								onClick={() => {
									deleteEvent(event.id);
								}}
							/>
							<Button
								as='a'
								color='teal'
								floated='right'
								content='View'
								onClick={() => {
									selectEvent(event);
								}}
							/>
						</div>
					</Segment>
				</Segment.Group>
			</>
		);
	}
}

export default EventListItem;
