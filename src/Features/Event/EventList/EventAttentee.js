import React, { Component } from 'react';
import { List, Image } from 'semantic-ui-react';

export default class EventAttentee extends Component {
	render() {
		const { attentee } = this.props;
		return (
			<>
				<List.Item>
					<Image as='a' size='mini' circular src={attentee.photoURL} />
				</List.Item>
			</>
		);
	}
}
