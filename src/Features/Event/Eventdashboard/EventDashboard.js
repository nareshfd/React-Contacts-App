import React, { Component } from 'react';
import { Grid, Button } from 'semantic-ui-react';
import EventList from '../EventList/EventList';
import EventForm from '../EventForm/EventForm';
import cuid from 'cuid';
// array obj
const eventsDbList = [
	{
		id: '1',
		title: 'Trip to Tower of London',
		date: '2018-03-27',
		category: 'culture',
		description:
			'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus sollicitudin ligula eu leo tincidunt, quis scelerisque magna dapibus. Sed eget ipsum vel arcu vehicula ullamcorper.',
		city: 'London, UK',
		venue: "Tower of London, St Katharine's & Wapping, London",
		hostedBy: 'Bob',
		hostPhotoURL: 'https://randomuser.me/api/portraits/men/20.jpg',
		attendees: [
			{
				id: 'a',
				name: 'Bob',
				photoURL: 'https://randomuser.me/api/portraits/men/20.jpg'
			},
			{
				id: 'b',
				name: 'Tom',
				photoURL: 'https://randomuser.me/api/portraits/men/22.jpg'
			}
		]
	},
	{
		id: '2',
		title: 'Trip to Punch and Judy Pub',
		date: '2018-03-28',
		category: 'drinks',
		description:
			'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus sollicitudin ligula eu leo tincidunt, quis scelerisque magna dapibus. Sed eget ipsum vel arcu vehicula ullamcorper.',
		city: 'London, UK',
		venue: 'Punch & Judy, Henrietta Street, London, UK',
		hostedBy: 'Tom',
		hostPhotoURL: 'https://randomuser.me/api/portraits/men/22.jpg',
		attendees: [
			{
				id: 'b',
				name: 'Tom',
				photoURL: 'https://randomuser.me/api/portraits/men/22.jpg'
			},
			{
				id: 'a',
				name: 'Bob',
				photoURL: 'https://randomuser.me/api/portraits/men/20.jpg'
			}
		]
	}
];

// array obj

class EventDashboard extends Component {
	state = {
		events: eventsDbList,
		isOpen: false,
		selectedEvent: null
	};
	handleCreateEvent = newEvent => {
		newEvent.id = cuid();
		newEvent.hostPhotoURL = '/assets/user.png';
		this.setState(({ events }) => {
			events = [...events, newEvent];
			// console.log('=============== dashboard=====================');
			// console.log(events);
			// console.log('====================================');
			return { events: events };
		});

		// this.setState(({events}) => ({ events: [...preState.events, newEvent] }));
	};

	// formtoggleHandler = () => {
	// 	this.setState({ isOpen: !this.state.isOpen });
	// };
	formtoggleHandler = () => {
		this.setState({
			isOpen: true,
			selectedEvent: null
		});
	};
	handleFormcancle = () => {
		this.setState({
			isOpen: false,
			selectedEvent: null
		});
	};
	handleSelectEvent = event => {
		this.setState({
			selectedEvent: event,
			isOpen: true
		});
	};
	handleUpdateEvent = updatedEvent => {
		this.setState(({ events }) => ({
			events: events.map(events => {
				if (events.id === updatedEvent.id) {
					return { ...updatedEvent };
				} else {
					return events;
				}
			}),
			isOpen: false,
			selectedEvent: null
		}));
	};

	handleDeleteEvent = id => {
		this.setState(({ events }) => ({
			events: events.filter(e => e.id !== id)
		}));
	};

	render() {
		const { events, isOpen, selectedEvent } = this.state;
		return (
			<div className='mt-5'>
				<Grid>
					<Grid.Column width={10}>
						<EventList
							events={events}
							selectEvent={this.handleSelectEvent}
							deleteEvent={this.handleDeleteEvent}
						/>
					</Grid.Column>
					<Grid.Column width={6}>
						<Button
							onClick={this.formtoggleHandler}
							positive
							content='Create Event'
						/>

						{isOpen && (
							<EventForm
								key={selectedEvent ? selectedEvent.id : 0}
								updateEvent={this.handleUpdateEvent}
								selectedEvent={selectedEvent}
								CVdata={this.handleCreateEvent}
								cancleFromOpen={this.handleFormcancle}
							/>
						)}
					</Grid.Column>
				</Grid>
			</div>
		);
	}
}

export default EventDashboard;
