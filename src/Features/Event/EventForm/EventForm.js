import React, { Component } from 'react';
import { Segment, Form, Button } from 'semantic-ui-react';

class EventForm extends Component {
	state = {
		title: '',
		date: '',
		city: '',
		hostedBy: '',
		venue: ''
	};

	componentDidMount() {
		if (this.props.selectedEvent != null) {
			this.setState({
				...this.props.selectedEvent
			});
		} else {
		}
	}

	HandleformSubmit = evt => {
		evt.preventDefault();

		if (this.state.id) {
			this.props.updateEvent(this.state);
		} else {
			this.props.CVdata(this.state);
		}
		// console.log(this.props.Create_Event(this.state));
	};
	OnchangeHandler = ({ target: { value, name } }) => {
		this.setState({
			[name]: value
		});
	};

	render() {
		const { cancleFromOpen } = this.props;
		const { title, date, city, hostedBy, venue } = this.state;

		return (
			<>
				<Segment>
					<Form onSubmit={this.HandleformSubmit}>
						<Form.Field>
							<label>Event Title</label>
							<input
								name='title'
								value={title}
								onChange={this.OnchangeHandler}
								placeholder='First Name'
							/>
						</Form.Field>
						<Form.Field>
							<label>Event Date</label>
							<input
								name='date'
								value={date}
								onChange={this.OnchangeHandler}
								type='date'
								placeholder='Event Date'
							/>
						</Form.Field>
						<Form.Field>
							<label>City</label>
							<input
								name='city'
								value={city}
								onChange={this.OnchangeHandler}
								placeholder='City event is taking place'
							/>
						</Form.Field>
						<Form.Field>
							<label>Venue</label>
							<input
								name='venue'
								value={venue}
								onChange={this.OnchangeHandler}
								placeholder='Enter the Venue of the event'
							/>
						</Form.Field>
						<Form.Field>
							<label>Hosted By</label>
							<input
								name='hostedBy'
								value={hostedBy}
								onChange={this.OnchangeHandler}
								placeholder='Enter the name of person hosting'
							/>
						</Form.Field>
						<Button positive type='submit'>
							Submit
						</Button>
						<Button type='button' content='Cancel' onClick={cancleFromOpen} />
					</Form>
				</Segment>
			</>
		);
	}
}

export default EventForm;
