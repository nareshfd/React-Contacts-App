import React, { Component } from "react";
import { connect } from "react-redux";
// import { IncrementCounter, DecrementCounter } from "./testActions";
import { Button } from "semantic-ui-react";
import { IncrementCounter, DecrementCounter } from "./testActions";

const mapStatePrpos = state => ({
	data: state.data
});

const mapDispatchprops = { IncrementCounter, DecrementCounter };

class testArea extends Component {
	render() {
		const { data, IncrementCounter, DecrementCounter } = this.props;
		return (
			<div className='mt-5'>
				<h1> testArea</h1> <p>{data}</p>
				<Button onClick={IncrementCounter} positive content='inc' />
				<Button onClick={DecrementCounter} negative content='dec' />
			</div>
		);
	}
}

export default connect(mapStatePrpos, mapDispatchprops)(testArea);
