import { Switch } from "react-router-dom";
import { Increment_Counter, Decrement_Counter } from "./testConstants";

const initialState = {
	data: 42
};

const testReducer = (state = initialState, action) => {
	switch (action.type) {
		case Increment_Counter:
			return { ...state, data: state.data + 2 };
		case Decrement_Counter:
			return { ...state, data: state.data - 2 };

		default:
			return state;
	}
};

export default testReducer;
