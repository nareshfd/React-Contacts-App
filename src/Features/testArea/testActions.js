import { Increment_Counter, Decrement_Counter } from "./testConstants";

export const IncrementCounter = () => {
	return {
		type: Increment_Counter
	};
};
export const DecrementCounter = () => {
	return {
		type: Decrement_Counter
	};
};
