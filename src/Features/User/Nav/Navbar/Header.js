import React, { Component } from 'react';
import Signedout from './Menus/Signedout';
import SignediMenu from './Menus/SigndinMenu';

import { Menu, Container, Button } from 'semantic-ui-react';
import logo from '../../../../img/logo.svg';
import { NavLink, Link, withRouter } from 'react-router-dom';
/* eslint-disable class-methods-use-this */
/* eslint-disable no-undef */
class Header extends Component {
	state = {
		authenticated: false
	};

	HandleSignIn = () => {
		this.setState({ authenticated: true });
	};
	HandleSignout = () => {
		this.setState({ authenticated: false });
		this.props.history.push('/');
	};
	render() {
		const { authenticated } = this.state;
		return (
			<Menu inverted fixed='top'>
				<Container>
					<Menu fixed='top' className='bg-primary'>
						<Container>
							<Menu.Item header as={NavLink} to='/'>
								<img src={logo} alt='logo' />
								{/* {branding} */}
							</Menu.Item>
							<Menu.Item position='left'>
								<Menu.Item name='Events' as={NavLink} to='/events' />

								<Button
									as={Link}
									to='/creaEvent'
									basic
									inverted
									content='Create Event'
									style={{ marginRight: '0.5em' }}
								/>
							</Menu.Item>

							{authenticated ? (
								<SignediMenu Signedout={this.HandleSignout} />
							) : (
								<Signedout SignedIn={this.HandleSignIn} />
							)}
						</Container>
					</Menu>
				</Container>
			</Menu>
		);
	}
}

export default withRouter(Header);
