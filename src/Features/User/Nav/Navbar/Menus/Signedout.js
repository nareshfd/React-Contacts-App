import React from 'react';
import { Menu, Button } from 'semantic-ui-react';
import { NavLink } from 'react-router-dom';

const Signedout = ({ SignedIn }) => {
	return (
		<Menu.Item position='right' as={NavLink} to='/events'>
			<Button basic inverted content='Login' onClick={SignedIn} />
			<Button
				basic
				inverted
				content='Register'
				style={{ marginLeft: '0.5em' }}
			/>
		</Menu.Item>
	);
};

export default Signedout;
