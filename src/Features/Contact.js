import React, { Component } from "react";
import "../App";
import PropTypes from "prop-types";

class Contact extends Component {
  static protoTyypes = {
    name: PropTypes.string.isRequired,
    Email: PropTypes.string.isRequired,
    phone: PropTypes.number.isRequired
  };

  state = {
    on: false
  };

  toggleBtn = function() {
    this.setState({
      on: !this.state.on
    });
  };

  render() {
    const { name, email, phone } = this.props;
    return (
      <div className="card card-body mb-3">
        <h4 className="list-group-itwm">
          Name: {name}{" "}
          <i className="fa fa-sort-down" onClick={this.toggleBtn.bind(this)} />
        </h4>
        <ul className="list-group">
          <li className="list-group-itwm"> Email: {email} </li>
          <li className="list-group-itwm"> phone number: {phone} </li>
        </ul>
      </div>
    );
  }
}

export default Contact;
