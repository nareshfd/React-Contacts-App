import React from 'react';
import {
	Segment,
	Container,
	Header,
	Image,
	Button,
	Icon
} from 'semantic-ui-react';
import { Link } from 'react-router-dom';

export default function HomePage({ History }) {
	return (
		<Segment
			inverted
			textAlign='center'
			vertical
			className='masthead mx-auto pt-5'>
			<Container text>
				<Header as='h1' inverted>
					<Image
						size='massive'
						src='/assets/logo.png'
						alt='logo'
						style={{ marginBottom: 12 }}
					/>
					Re-vents
				</Header>
				<Button size='huge' inverted as={Link} to='/events'>
					Get started
					<Icon
						onClick={() => {
							History.push('/events');
						}}
						name='right arrow'
						inverted
					/>
				</Button>
			</Container>
		</Segment>
	);
}
